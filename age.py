from agent import Ga
from ambient import Ambient
from agent import AgentP
import numpy as np


def two_best(p):
    # print(len(p), type(p[9]))
    one = [0, 0]
    two = [0, 0]
    for i in p:
        if i[1] > one[1]:
            one = i
        elif i[1] > two[1]:
            two = i
    return [one[0], two[0]]


class Age:
    K = 9
    Q = 100
    epoch = 0

    islands = []
    top_islands = []
    top_two = []

    def __init__(self):
        self.islands = [Ga() for e in range(self.K)] + [AgentP()]

    def next_epoch(self):
        result = []
        # print('epoch:', str(self.epoch) + '/' + str(self.Q))
        self.epoch += 1
        for i in self.islands:
            i.evolution()
        for i in self.islands:
            result.append(i.best_fit())
        tops = two_best(result)
        self.top_islands = result
        for i in self.islands:
            if type(i) == Ga:
                i.population = i.population + tops
            else:
                # print("tops = ", tops)
                i.population = np.array([tops[0]])
        self.top_two = tops
        # print(tops[0], Ambient.gain(tops[0]))
