from flask import Flask
from age import Age
import json
from ambient import Ambient

app = Flask(__name__)
age = Age()
age.next_epoch()


@app.route('/reboot')
def reboot():
    global age
    age = Age()
    age.next_epoch()
    return json.dumps(['ok'])


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/epoch')
def epoch():
    return str(age.epoch)


@app.route('/island/<id>')
def island(id):
    # print('id:', id)
    return json.dumps(age.top_islands[int(id)])


@app.route('/islands')
def islands():
    # print('id:', age.top_islands)

    return json.dumps(age.top_islands)


@app.route('/next')
def next():
    age.next_epoch()
    return json.dumps(['ok'])


@app.route('/top')
def top():
    gains = [i[1] for i in age.top_islands]
    configs = [i[0] for i in age.top_islands]
    # print(len(age.top_islands))
    # print([gains, configs])
    return json.dumps([gains, configs])


if __name__ == '__main__':
    app.run()
