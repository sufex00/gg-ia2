import QtQuick 2.5
import QtQuick.Controls 1.4
import "qrc:/scripts.js" as JS
import QtQuick.Layouts 1.3

ApplicationWindow {
    function hpercent(ref,percent) { return (ref.height/100)*percent; }
    function wpercent(ref,percent) { return (ref.width/100)*percent; }

    id: root
    height: 600
    width: 1200
    visible: true
    title: "IA2"

    property int radSize: 5
    property alias epochCount: epochCount


    Text {
        id: name
        text: qsTr("")
        anchors.right: root.right
    }


    ColumnLayout{
        Item{
            height: hpercent(root, 40)
            width: wpercent(root, 100)
            RowLayout{
                anchors.fill: parent
                anchors.centerIn: parent

                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i1

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i2

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i3

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i4

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i5

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i6

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i7

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i8

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i9

                }
                Island{
                    height: wpercent(parent, radSize)
                    width: height
                    id: i10
                    color: 'blue'

                }

            }
        }
        Item{
            height: hpercent(root, 10)
            width: wpercent(root, 100)
            RowLayout{
                anchors.fill: parent
                Item{
                    height: hpercent(parent, 100)
                    width: wpercent(parent, 50)
                    Button{
                        anchors.centerIn: parent
                        text: "next epoch"
                        onClicked: {
                            JS.get('http://127.0.0.1:5000/next')
                            epochCount.text = parseInt(epochCount.text) + 1
                            JS.top()
                        }
                    }
                }
                //                Button{
                //                    text: "load"
                //                    onClicked: {
                //                        JS.reloadCicles()
                //                    }
                //                }
                Item{
                    height: hpercent(parent, 100)
                    width: wpercent(parent, 50)
                    Button{
                        anchors.centerIn: parent
                        text: "boot/reboot"
                        onClicked: {
                            JS.reboot()
                            epochCount.text = 1
//                            JS.top()
                        }
                    }
                }
            }
        }
        Item{
            height: hpercent(root, 30)
            width: wpercent(root, 100)
            Item{
                height: hpercent(root, 30)
                width: wpercent(root, 50)
                anchors.centerIn: parent
                ColumnLayout{
                    anchors.fill: parent
                    Row{
                        height: hpercent(parent, 25)
                        width: wpercent(parent, 100)
                        Item{
                            height: hpercent(parent, 100)
                            width: wpercent(parent, 50)
                            Row{
                                anchors.fill: parent
                                Item{
                                    height: hpercent(parent, 100)
                                    width: wpercent(parent, 50)
                                    Text{
                                        anchors.centerIn: parent
                                        text: "Epoch:"
                                    }
                                }
                                Item{
                                    height: hpercent(parent, 100)
                                    width: wpercent(parent, 50)
                                    Text{
                                        id: epochCount
                                        anchors.centerIn: parent
                                        text: '1'
                                    }
                                }
                            }

                        }
                        Item{
                            height: hpercent(parent, 100)
                            width: wpercent(parent, 50)
                            Row{
                                anchors.fill: parent
                                Item{
                                    height: hpercent(parent, 100)
                                    width: wpercent(parent, 50)
                                    Text{
                                        anchors.centerIn: parent
                                        text: "Last best gain:"
                                    }
                                }
                                Item{
                                    height: hpercent(parent, 100)
                                    width: wpercent(parent, 50)
                                    Text{
                                        id: bestGain
                                        anchors.centerIn: parent
                                        text: '0.00'
                                    }
                                }
                            }

                        }
                    }
                    Row{
                        height: hpercent(parent, 25)
                        width: wpercent(parent, 100)
                        Item{
                            height: hpercent(parent, 100)
                            width: wpercent(parent, 50)
                            Text{
                                anchors.centerIn: parent
                                text: "Configuration:"
                            }
                        }
                        Item{
                            height: hpercent(parent, 100)
                            width: wpercent(parent, 50)
                            Text{
                                id: config
                                anchors.centerIn: parent
                                text: '[0,0,0,0,0,0]'
                            }
                        }

                    }
//                    Row{
//                        height: hpercent(parent, 25)
//                        width: wpercent(parent, 100)
//                        Rectangle{
//                            height: hpercent(parent, 100)
//                            width: wpercent(parent, 50)
//                            color:'white'
//                        }
//                        Rectangle{
//                            height: hpercent(parent, 100)
//                            width: wpercent(parent, 50)
//                            color:'red'
//                        }
//                    }
//                    Row{
//                        height: hpercent(parent, 25)
//                        width: wpercent(parent, 100)
//                        Rectangle{
//                            height: hpercent(parent, 100)
//                            width: wpercent(parent, 50)
//                            color:'white'
//                        }
//                        Rectangle{
//                            height: hpercent(parent, 100)
//                            width: wpercent(parent, 50)
//                            color:'red'
//                        }
//                    }
                }
            }
        }
    }
}
