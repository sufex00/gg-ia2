import QtQuick 2.0
import "qrc:/scripts.js" as JS

Item {
    property alias internalCicle: internalCicle
    property alias numericValue: numericValue
    property alias color: internalCicle.color
    property int auxHeight: wpercent(parent, 8)
    Rectangle{
        height: auxHeight
        width: height
        radius: width/2
        border.color: 'black'
        Rectangle{
            id:internalCicle
            anchors.centerIn: parent
            height: hpercent(parent, 0)
            width: height
            radius: width/2
            color: 'red'
        }
        Text {
            anchors.centerIn: parent
            id: numericValue
            text: qsTr("0.0")
        }
    }

    onAuxHeightChanged: {
        JS.reloadCicles()
    }
}
