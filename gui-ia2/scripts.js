function get(url) {
    name.text = "[\"loading...\"]"
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
            print('HEADERS_RECEIVED')
        } else if(xhr.readyState === XMLHttpRequest.DONE) {
            print('DONE')
            var json = JSON.parse(xhr.responseText.toString())
//            console.log(json.items)
//            console.log(xhr.responseText.toString())
            name.text = xhr.responseText.toString()
            reloadCicles()
        }
    }
    xhr.open("GET", url);
    xhr.send();
}


function reloadCicles(){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
            print('HEADERS_RECEIVED')
        } else if(xhr.readyState === XMLHttpRequest.DONE) {
            print('DONE')
            var json = JSON.parse(xhr.responseText.toString())
            i1.internalCicle.height = (json[0][1]/32)*i1.auxHeight
            i2.internalCicle.height = (json[1][1]/32)*i2.auxHeight
            i3.internalCicle.height = (json[2][1]/32)*i3.auxHeight
            i4.internalCicle.height = (json[3][1]/32)*i4.auxHeight
            i5.internalCicle.height = (json[4][1]/32)*i5.auxHeight

            i6.internalCicle.height = (json[5][1]/32)*i6.auxHeight
            i7.internalCicle.height = (json[6][1]/32)*i7.auxHeight
            i8.internalCicle.height = (json[7][1]/32)*i8.auxHeight
            i9.internalCicle.height = (json[8][1]/32)*i9.auxHeight
            i10.internalCicle.height = (json[9][1]/32)*i10.auxHeight

            i1.numericValue.text = parseFloat(json[0][1]).toFixed(2)
            i2.numericValue.text = parseFloat(json[1][1]).toFixed(2)
            i3.numericValue.text = parseFloat(json[2][1]).toFixed(2)
            i4.numericValue.text = parseFloat(json[3][1]).toFixed(2)
            i5.numericValue.text = parseFloat(json[4][1]).toFixed(2)

            i6.numericValue.text = parseFloat(json[5][1]).toFixed(2)
            i7.numericValue.text = parseFloat(json[6][1]).toFixed(2)
            i8.numericValue.text = parseFloat(json[7][1]).toFixed(2)
            i9.numericValue.text = parseFloat(json[8][1]).toFixed(2)
            i10.numericValue.text = parseFloat(json[9][1]).toFixed(2)
            top()
        }
    }
    xhr.open("GET", "http://127.0.0.1:5000/islands");
    xhr.send();
}

function reboot(){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
            print('HEADERS_RECEIVED')
        } else if(xhr.readyState === XMLHttpRequest.DONE) {
            print('DONE')
            reloadCicles()
        }
    }
    xhr.open("GET", "http://127.0.0.1:5000/reboot");
    xhr.send();
}

function top(){
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
            print('HEADERS_RECEIVED')
        } else if(xhr.readyState === XMLHttpRequest.DONE) {
            print('DONE')
//            console.log(xhr.responseText)
            var json = JSON.parse(xhr.responseText.toString())
//            console.log(json)
            var gains = json[0]
            var configs = json[1]
            var mx = Math.max.apply(null, gains) // 4
//            print(typeof(gains[0]))
//            console.log(mx)
            var id = gains.indexOf(mx)
            bestGain.text = mx.toFixed(2)
            config.text = '[' + configs[id].toString() + ']'
        }
    }
    xhr.open("GET", "http://127.0.0.1:5000/top");
    xhr.send();
}
