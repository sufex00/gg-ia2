import random
import functools

from ambient import Ambient

import numpy as np


def fitness(chromo):
    result = Ambient.gain(chromo)
    return result


def chromo_cmp(a, b):
    if fitness(a) > fitness(b):
        return 1
    elif fitness(a) == fitness(b):
        return 0
    else:
        return -1


class Ga:
    population = []
    prob_crossover = 1.0
    prob_mutation = 0.5
    gen = 1
    gen_size = 50
    top = 0.10

    def __init__(self):
        self.initial()

    def initial(self):
        p = []
        while len(p) != 1000:
            current = [random.randint(0, 360) for j in range(6)]
            if not current in p:
                p.append(current)
        population_initial = p
        random.shuffle(population_initial)
        self.population = population_initial[:self.gen_size]

    def mutation(self, chromosome):
        chromosome[random.randint(0, 5)] = random.randint(0, 360)
        chromosome[random.randint(0, 5)] = random.randint(0, 360)
        return chromosome

    def selection(self, partial_population):
        groups = self.cut(partial_population, (self.gen_size / 2.0) / len(partial_population))
        return groups[0] + random.sample(population=groups[1], k=int(self.gen_size / 2))

    def crossover(self, parents):
        cut = random.randint(0, 5)
        father, mother = parents
        child1 = father[cut:] + mother[:cut]
        child2 = father[:cut] + mother[cut:]
        return [child1, child2]

    def cut(self, partial_population, rate):
        partial_population = sorted(partial_population, key=functools.cmp_to_key(chromo_cmp))
        mid = int(len(partial_population) * rate)
        result = [partial_population[-mid:], partial_population[:mid]]
        return result

    # avanca para proxima geracao
    def next_generation(self):
        partial_best = self.cut(self.population, 0.5)[0]  # extrai 50% melhores da geracao anterior
        partial_all = self.population
        new_gen = partial_best[int(
            len(partial_best) * (1.0 - self.top)):]  # adicionar os 5% melhores de 'partial_best' para proxima geracao
        if random.random() < self.prob_crossover:
            # fazer cruzamento par a par 3 vezes para os melhores, antes de cada cruzamento embaralhamos os pares
            for k in range(3):
                length = int(len(partial_best) / 2)
                random.shuffle(partial_best)
                for j in range(0, length):
                    new_gen = new_gen + self.crossover(partial_best[j:j + 2])
            # fazer cruzamento par a par 3 vezes para toda geracao anterior, antes de cada cruzamento embaralhamos os pares
            for k in range(3):
                length = int(len(partial_all) / 2)
                random.shuffle(partial_all)
                for j in range(0, length):
                    new_gen = new_gen + self.crossover(partial_all[j:j + 2])  # OK
        # dentre a nova geracao, a nova populacao sera os 50% melhores da nova geracao, juntamente com 50% aleatorio, dado que populacao tera tamanho 'gen_size'
        # mutacao eh feita trocando 2 genes aleatorios de 1 cromossomo
        for j in range(len(new_gen)):
            if random.random() < self.prob_mutation:
                new_gen[j] = self.mutation(new_gen[j])
        self.population = self.selection(new_gen)  # OK

    def best_fit(self):
        fit = -float('inf')
        chromo = []
        for i in self.population:
            current_fit = fitness(i)
            if fit < current_fit:
                fit = current_fit
                chromo = i
        return [chromo, fit]

    # avanca k geracoes sendo k = 'gen'
    def evolution(self):
        for i in range(self.gen):
            self.next_generation()


class AgentP:
    population = []
    k = 20
    beta1 = 0.085
    beta2 = 0.995
    m = 0
    v = 0
    eps = 1e-4
    gamma = 0.8

    def __init__(self):
        self.population.append([random.randint(0, 360) for j in range(6)])

    def dev(self, x, dire):
        h = 1.0
        x[dire] = x[dire] + h
        v_h = fitness(x)
        x[dire] = x[dire] - 2 * h
        v = fitness(x)
        d = v_h - v
        x[dire] = x[dire] + h
        return d / (2.0 * h)

    def grad(self, x):
        g = np.zeros(6)
        for e in range(6):
            g[e] = self.dev(x, e)
        return g

    def next_generation(self):
        cur_x = self.population[0]
        # print(self.population)
        # print('curr', cur_x)
        prev_x = cur_x
        for e in range(6):
            if prev_x[e] < 1:
                prev_x[e] = 1

        grad_v = self.grad(cur_x)
        self.m = self.beta1 * self.m + (1 - self.beta1) * grad_v
        self.v = self.beta2 * self.v + (1 - self.beta2) * (grad_v * grad_v)

        cur_x = cur_x + self.gamma * self.m / (np.sqrt(self.v) + self.eps)
        self.population[0] = cur_x

    def evolution(self):
        self.m = 0
        self.v = 0

        for i in range(self.k):
            self.next_generation()

    def best_fit(self):
        fit = fitness(self.population[0])
        return [self.population[0].astype(int).tolist(), fit]
