import math


def norm(x):
    return x / 36.0 - 5.0


class Ambient:
    def __init__(self):
        pass

    # avaliacao do cromossomo
    @staticmethod
    def gain(chromo):
        phi1 = int(chromo[0]) % 360
        theta1 = int(chromo[1]) % 360
        phi2 = int(chromo[2]) % 360
        theta2 = int(chromo[3]) % 360
        phi3 = int(chromo[4]) % 360
        theta3 = int(chromo[5]) % 360

        x = norm(float(phi1))
        y = norm(float(theta1))
        gain1 = math.sin(x) + math.cos(y)
        x = norm(float(phi2))
        y = norm(float(theta2))
        gain2 = y * math.sin(x) - x * math.cos(y)
        x = norm(float(phi3))
        y = norm(float(theta3))
        r = math.sqrt(x * x + y * y)
        gain3 = math.sin(x * x + 3.0 * y * y) / (0.1 + r * r) + (x * x + 5.0 * y * y) * math.exp(1.0 - r * r) / 2.0
        return 4.0 * gain1 + gain2 + 4.0 * gain3
